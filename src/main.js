import Vue from 'vue/dist/vue.js'
import VueFire from 'vuefire'
import App from './App.vue'

Vue.use(VueFire)

new Vue({
  el: '#app',
  template: '<App/>',
  components: { 
    App
  }
})